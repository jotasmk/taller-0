import random

# Genera una lista de 48 elementos que pueden tomar valores entre 1 y 12.
def generarMazo():
    l=list(range(1,13)) #genera lista del 1 al 12
    mazo=l+l+l+l #pega las 4 listas para tener los 4 palos
    random.shuffle(mazo) #mezcla el mazo
    return mazo


def jugar(a):
    count=0
    while count < 21 and len(a) > 0:
        count = count + a.pop(0)
    return count


def jugarVarios(a,n):
    res=[]
    for i in range(1,n+1):
        res.append(jugar(a))
    return res


def verQuienGano(res):
    i = 0 
    hayganador = False
    while hayganador == False:
        if res[i] == 21:
            hayganador = True
            ganador = i
        else:
            if i < (len(res)-1):
                i = i + 1
            else:
                hayganador = True
                ganador = "No ganó nadie"
    return ganador
    
a=generarMazo()
b=jugarVarios(a,4)
c=verQuienGano(b)
print("Este programa simula una partida de BlackJack entre cuatro jugadores (llamados Jugador 1, 2, 3 y 4) e indica quién de ellos ganó.")
if c == "No ganó nadie":
    print(c)
else:
    print("Ganó el Jugador",c)


